import os

def install(package:list, module:str, user:str, provider:str):
    if not os.path.exists(module):
        os.mkdir(module)
        os.system("touch %s/__init__.py" % module)
    for pack in package:
        if os.path.exists("{0}/{0}_{1}".format(module, pack)) == False:
            os.system("git clone https://%s/%s/%s%s" % (provider, user, module, pack))
            if os.path.exists("%s%s/build.sh" % (module, pack)):
                os.system("cd %s%s; sh build.sh" % (module, pack))
            os.system("cp -r {1}{0}/{1}/{1}_* {1}".format(pack, module))
            os.system("rm -rf {1}{0}".format(pack, module))
        